FROM tomcat:alpine

COPY web/target/time-tracker-web-*.war /usr/local/tomcat/webapps/ROOT.war