pipeline {
    agent any
     tools {
         maven "Maven 2"
     }
    environment {
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "10.106.241.168"
        NEXUS_REPOSITORY = "maven-repo"
        NEXUS_CREDENTIAL_ID = "nexus-credentials"
    }
    stages {
        stage("Clone Code") {
            steps {
                script {
                    // Let's clone the source
                    git 'https://gitlab.com/AlexeiBarabash/hometask.git';
                }
            }
        }
        stage("SonarQube Tests") {
                // Let's run tests in Sonar
            environment {
                scannerHome = tool 'SonarQubeScanner' 
            }
            steps {
                withSonarQubeEnv('sonarqube') {
                    sh "echo ${scannerHome}"
                    sh "${scannerHome}/bin/sonar-scanner"
                }
                timeout(time: 10, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
        }
        }
        stage("MVN Build") {
            steps {
                script {
                    // Now Let's Build the Package
                    sh "mvn package -DskipTests=true"
                }
            }
        }
        stage("Publish Artifact to Nexus") {
            steps {
                script {
                    // Now Let's Deploy the WAR artifact
                    pom = readMavenPom file: "pom.xml";
                    // Find built artifact under target folder
                    filesByGlob = findFiles(glob: "web/target/*.war");
                    // Print some info from the artifact found
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                    // Extract the path from the File found
                    //echo "${filesByGlob}"
                    artifactPath = filesByGlob[0].path;
                    // Assign to a boolean response verifying If the artifact name exists
                    artifactExists = fileExists artifactPath;
                    if(artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";
                        nexusArtifactUploader(
                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: artifactPath,
                                type: 'war']
                            ]
                        );
                    } else {
                        error "*** File: ${artifactPath}, could not be found";
                    }
                }
            }
        }
        stage("Build and Push Container"){
            steps{
                // Now Let's Build the Container and Push it to Nexus
                sh "docker build -t 10.106.241.168:5000/time-tracker:${env.BUILD_NUMBER} ."
                sh "docker push 10.106.241.168:5000/time-tracker:${env.BUILD_NUMBER}"
            }
        }
    }
    post { 
        always { 
            cleanWs()
        }
    }
}